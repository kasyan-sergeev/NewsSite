<?php

use yii\helpers\Html;

$this->title = 'News';
?>

<h1>News</h1> 
<?= Html::a('Add news', ['news/create'], ['class' => 'btn btn-primary', 'role' => 'button']) ?>

<?= Html::beginForm(['', 'id' => $id], 'get') ?>
<span>Sort by 
    <?= Html::dropDownList('sort', '', ['likes' => 'likes', 'date' => 'date'], ['prompt' => '']) ?>
    Category
    <?= Html::dropDownList('category', '', $categories, ['prompt' => '']) ?>
    <?= Html::submitButton('ok', ['class' => 'submit btn btn-secondary btn-sm']) ?>
</span>
<?= Html::endForm() ?>
<?php
if (count($news) == 0) {
    echo '<h3>Such news not found</h3>';
} else {
    foreach ($news as $new):
        ?>

        <h3><?= Html::a(Html::encode("{$new->title}"), ['news/view', 'id' => "$new->id"]) ?></h3>
        <?= Html::img(Html::encode("$GLOBALS[HTTP_HOST]/uploads/{$new->img}"), ['width' => '360px']) ?><br>
        <?= Html::encode("{$new->description}") ?>
        <?= Html::a('Read more...', ['news/view', 'id' => "$new->id"]) ?>
        <br>
        <?php
        if ($new->likes != NULL) {
            echo '<i class="fa fa-thumbs-up" aria-hidden="true"></i>';
            echo Html::encode("{$new->likes}");
        }
        ?>
        <i class="fa fa-clock-o" aria-hidden="true"></i>
        <?= Html::encode(date('H:i', strtotime("$new->date"))) ?> 
        <i class="fa fa-calendar" aria-hidden="true"></i> 
        <?= Html::encode(date('d.m.Y ', strtotime("$new->date"))) ?> 

    <?php endforeach;
} ?>
