<?php

use yii\helpers\Html;

$this->title = Html::encode("{$info->title}");
?>

<h2>"<?= $info->title ?>"</h2>
<?= Html::img(Html::encode("$GLOBALS[HTTP_HOST]/uploads/{$info->img}"), ['width' => '50%', 'style' => 'float:left']) ?>
<?= Html::encode("{$info->content}"); ?>
<br>
<?php
if ($info->likes != NULL) {
    echo '<i class="fa fa-thumbs-up" aria-hidden="true"></i> ';
    echo Html::encode("{$info->likes}");
}
?>
<i class = "fa fa-clock-o" aria-hidden = "true"></i> 
<?= Html::encode(date('H:i', strtotime("$info->date"))) ?>
<i class = "fa fa-calendar" aria-hidden = "true"></i>
<?= Html::encode(date('d.m.Y ', strtotime("$info->date"))) ?>
<?= Html::beginForm(['', 'id' => "$info->id", 'like' => 'yes'], 'get') ?>
<?= Html::submitButton('I like it!', ['class' => 'btn btn-success']) ?>
<?= Html::endForm() ?>