<?php

namespace app\controllers;

use Yii;
use app\models\News;
use app\models\Categories;
use app\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Image;
use yii\helpers\ArrayHelper;

class NewsController extends Controller {

    public function actionIndex() {
        $searchModel = new NewsSearch();
        $news = $searchModel->search();

        return $this->render('index', [
                    'news' => $news, 'categories' => $this->getCategoryList(),
        ]);
    }

    public function actionView($id) {
        if (Yii::$app->request->get('like') == 'yes') {
            $news = News::find()->where('id=:id', ['id' => $id])->one();
            $likes = $news['likes'] + 1;
            $news = News::updateAll(['likes' => "$likes"], ['id' => "$id"]);
        }
        return $this->render('view', [
                    'info' => $this->findNews($id),
        ]);
    }

    public function actionCreate() {

        $model = new News();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'img');
            $img = Image::uploadImage($file);
            $model->setImage($img);
            var_dump($model->attributes);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
                    'model' => $model, 'items' => $this->getCategoryList(),
        ]);
    }

    protected function findNews($id) {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findCategories($id) {
        if (($categories = Categories::findOne($id)) !== null) {
            return $categories;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function getCategoryList() {
        $categories = Categories::find()->all();
        $items = ArrayHelper::map($categories, 'id', 'category');
        return $items;
    }

}
