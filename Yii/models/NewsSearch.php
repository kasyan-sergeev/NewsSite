<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form of `app\models\News`.
 */
class NewsSearch extends News {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'likes'], 'integer'],
            [['title', 'description', 'content', 'date', 'img', 'category_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search() {
        $query = News::find();
        if (Yii::$app->request->get('sort') == 'date' || Yii::$app->request->get('sort') == NULL) {
            $query = $query->orderBy('date DESC');
        } else {
            $query = $query->orderBy('likes DESC');
        }
        if (!empty(Yii::$app->request->get('category'))) {
            $query = $query->where('category_id=:category_id', ['category_id' => Yii::$app->request->get('category')]);
        }
        $news = $query->all();
        return $news;
    }

}
