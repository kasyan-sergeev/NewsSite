<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class Image extends Model {

    public static function uploadImage(UploadedFile $file) {
        echo $fileName = strtolower(md5(uniqid($file->baseName))).'.'."$file->extension";
        $file->saveAs(Yii::getAlias('@web').'uploads/'.$fileName);
        return $fileName;
    }

}
