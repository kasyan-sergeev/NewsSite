<?php

namespace app\models;

class News extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'description', 'content'], 'required'],
            [['title', 'description', 'content'], 'string'],
            [['date'], 'date', 'format' => 'php:Y-m-d H:i:s', 'message' => 'Time format looks like this: 2000-12-31 23:59:59 or leave it blank for the present time'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],
            [['title'], 'string', 'max' => 255],
            [['img'], 'image', 'extensions' => 'png, jpg', 'message' => 'sdad'],
            [['category_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'date' => 'Date',
            'img' => 'Image',
            'likes' => 'Likes',
            'category_id' => 'Category ID',
        ];
    }

    public function setImage($img) {
        $this->img = $img;
    }

}
