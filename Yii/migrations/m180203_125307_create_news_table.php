<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180202_125307_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'content' => $this->text(),
            'date' => $this->dateTime(),
            'img' => $this->string(),
            'likes' => $this->integer(),
            'category_id' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
